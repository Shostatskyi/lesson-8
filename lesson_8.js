var studentsAndPoints = [
  'Алексей Петров', 0,
  'Ирина Овчинникова', 60, 
  'Глеб Стукалов', 30,
  'Антон Павлович', 30, 
  'Виктория Заровская', 30,
  'Алексей Левенец', 70,
  'Тимур Вамуш', 30,
  'Евгений Прочан', 60,
  'Александр Малов', 0
];
 
/*
  1. Создать два массива students и points и заполнить их данными из studentsAndPoints без использования циклов.
  В первом массиве фамилии студентов, во втором баллы. Индексы совпадают.
*/

var students = studentsAndPoints.filter (function (studentsAndPoints) {
  if (isNaN(studentsAndPoints)) {
    return true;
  }
});
 
var points = studentsAndPoints.filter (function (studentsAndPoints) {
  if(!isNaN(studentsAndPoints)) {
    return true;
  }
});
 
// 2. Вывести список студентов без использования циклов в следующем виде:

console.log('Список студентов: ');
students.forEach (function (student, index) {
  console.log('Студент %s набрал %d баллов', student, points[index]);
});
 
// 3. Найти студента набравшего наибольшее количество баллов, и вывести информацию в формате:
 
function showMax () {
  var max = Math.max.apply(null, points);
  console.log('\nСтудент набравший максимальный балл: '+students[points.indexOf(max)]+' ('+max+' баллов)');
}
 
showMax();
 
// 4. Используя метод массива map увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30.
 
points = students.map(function (name, i) {
  if (name === 'Ирина Овчинникова') {
    return points[i]+30;
  }
  else if (name === 'Александр Малов') {
    return points[i]+30;
  }
  else return points[i];
});

 
/*
  Дополнительное задание: 
  реализовать функцию getTop, которое принимает на вход число и возвращает массив в формате studentsAndPoints, в 
  котором соответствующее кол-во студентов с лучшими баллами в порядке убывания кол-ва баллов. Используя эту функцию вывести в консоль 
  следующую информацию:
*/
 
function getTop (num) {
  var tmp = points.slice(), stdAndPnt = [], max;
  if (num > points.length) num = points.length;
  while (num) {
    max = Math.max.apply(null, tmp);
    stdAndPnt.push(students[tmp.indexOf(max)]);
    stdAndPnt.push(max);
    tmp[tmp.indexOf(max)]=0;
    num--;
  }
  return stdAndPnt;
}
 
function showTop(num) {
  var newArr=getTop(num), size = newArr.length; 
  console.log('\nТоп '+num+':');
  for (var i = 0; i <  size; i += 2) {
    console.log(newArr[i]+' - '+newArr[i+1]+' баллов');
  }
}
 
showTop(3);
showTop(5);